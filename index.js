function soDuong(x) {
  if (x < 0) {
    return false;
  } else {
    return true;
  }
}
function soChan(x) {
  if (x % 2 == 0) {
    return true;
  } else {
    return false;
  }
}
var numArr = [];
function getEl(n) {
  return document.getElementById(n);
}
function themSo() {
  var num = getEl("txt-input").value * 1;
  numArr.push(num);
  getEl("listnumber").innerHTML = numArr;
}

function tongSoDuong() {
  var tong = 0;
  for (var i = 0; i < numArr.length; i++) {
    if (soDuong(numArr[i])) {
      tong += numArr[i];
    }
  }
  getEl("rstongsoduong").innerHTML = "Tổng số dương: " + tong;
}
function demSoDuong() {
  var count = 0;
  for (var i = 0; i < numArr.length; i++) {
    if (soDuong(numArr[i])) {
      count++;
    }
  }
  getEl("rsdemsoduong").innerHTML = "Đếm số dương: " + count;
}
function soNhoNhat() {
  var min = numArr[0];
  for (var i = 1; i < numArr.length; i++) {
    if (min > numArr[i]) {
      min = numArr[i];
    }
  }
  getEl("rssonhonhat").innerHTML = "Số nhỏ nhất: " + min;
}
function soDuongNhoNhat() {
  var tempArr = [];
  for (var i = 0; i < numArr.length; i++) {
    if (numArr[i] > 0) {
      tempArr.push(numArr[i]);
    }
  }
  var e = tempArr[0];
  for (var j = 1; j < tempArr.length; j++) {
    if (e > tempArr[j]) {
      e = tempArr[j];
    }
  }
  getEl("rssoduongnhonhat").innerHTML = e;
}
function soChanCuoiCung() {
  var tempArr = [];
  for (var i = 0; i < numArr.length; i++) {
    if (soChan(numArr[i])) {
      tempArr.push(numArr[i]);
    }
  }
  if (tempArr.length < 0) {
    getEl("rssochancuoicung").innerHTML = "-1";
  } else {
    var e = tempArr[tempArr.length - 1];
    getEl("rssochancuoicung").innerHTML = e;
  }
}
function swap(a, b) {
  var temp = numArr[a];
  numArr[a] = numArr[b];
  numArr[b] = temp;
}
function doiChoViTri() {
  swap(getEl("inputIndex1").value, getEl("inputIndex2").value);
  getEl("rsdoichovitri").innerHTML = "Vị trí sau khi đổi chổ: " + numArr;
}
function sapXep() {
  for (var i = 0; i < numArr.length; i++) {
    for (var j = 0; j < numArr.length - 1; j++) {
      if (numArr[j] > numArr[j + 1]) {
        swap(j, j + 1);
      }
    }
  }
}
function soNguyenTo(x) {
  if (x < 2) {
    return false;
  }
  for (var i = 2; i <= Math.sqrt(x); i++) {
    if (x % i == 0) {
      return false;
    }
  }
  return true;
}
function timSoNguyenDauTien() {
  var x = -1;
  for (var i = 0; i < numArr.length; i++) {
    if (soNguyenTo(numArr[i])) {
      x = numArr[i];
      break;
    }
  }
  getEl("rssonguyentodautien").innerHTML = "Số nguyên tố đầu tiên: " + x;
}
var arrFloat = [];
function getFloatArray() {
  var n = getEl("txt-input2").value * 1;
  arrFloat.push(n);
  getEl("rsfloatnum").innerHTML = arrFloat;
}
function demSoNguyen() {
  var dem = 0;
  for (var i = 0; i < arrFloat.length; i++) {
    if (Number.isInteger(arrFloat[i])) {
      dem++;
    }
  }
  getEl("rsdemsonguyen").innerHTML = "Số nguyên: " + dem;
}
function soSanhSo() {
  var a = 0,
    b = 0;
  for (var i = 0; i < numArr.length; i++) {
    if (soDuong(numArr[i])) {
      a++;
    } else {
      b++;
    }
  }
  if (a > b) {
    getEl("rssosanhso").innerHTML =
      "Số dương nhiều hơn số âm :" + a + " > " + b;
  } else if (b < a) {
    getEl("rssosanhso").innerHTML =
      "Số âm nhiều hơn số dương :" + b + " > " + a;
  } else {
    getEl("rssosanhso").innerHTML = "Số âm bằng số dương :" + b + " = " + a;
  }
}
